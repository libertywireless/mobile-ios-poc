//
//  ViewController3.swift
//  navigatinBar
//
//  Created by Arun Sivakumar on 1/2/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let backItem = UIBarButtonItem()
//        backItem.title = "My title"
//        self.navigationController!.navigationBar.topItem!.title = "Back-"
//        self.navigationItem.backBarButtonItem?.target = self
//        self.navigationItem.backBarButtonItem?.action = #selector(handle)
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.bordered, target: self, action: "back:")
        self.navigationItem.leftBarButtonItem = newBackButton

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handle(){
        print("Hello")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
