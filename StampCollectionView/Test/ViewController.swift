//
//  ViewController.swift
//  Test
//
//  Created by Mario Antonio Cape on 2/22/18.
//  Copyright © 2018 Mario Antonio Cape. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let stampCollectionView = StampCollectionView(frame: CGRect(x: 0, y: 120, width: self.view.bounds.size.width, height: 40))
        stampCollectionView.backgroundColor = UIColor.red
        stampCollectionView.totalCount = 10
        stampCollectionView.selectedCount = 5
        self.view.addSubview(stampCollectionView)
        stampCollectionView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

