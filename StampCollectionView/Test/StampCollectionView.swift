//
//  StampCollectionView.swift
//  Test
//
//  Created by Mario Antonio Cape on 2/22/18.
//  Copyright © 2018 Mario Antonio Cape. All rights reserved.
//

import UIKit

class StampCollectionView: UIView {
    
    private var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    var totalCount: Int = 0
    var selectedCount: Int = 0
    var cellIdentifier = "UICollectionViewCell"
    var itemSize = CGSize(width: 20, height: 20)
    var lineSpacing: CGFloat = 4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        self.collectionView.register(StampViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = UIColor.yellow
        
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        self.addSubview(self.collectionView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = (CGFloat(totalCount) * itemSize.width) + (lineSpacing * CGFloat(totalCount - 1))
        self.collectionView.frame = CGRect(x: 0, y: 0, width: width, height: self.bounds.size.height)
    }
    
    func reloadData() {
        self.collectionView.reloadData()
    }
    
}

extension StampCollectionView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalCount;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! StampViewCell
        cell.size = self.itemSize
        cell.isSelected = indexPath.row < selectedCount
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}


private class StampViewCell: UICollectionViewCell {
    var stampView = UIView()
    var size: CGSize = CGSize.zero {
        didSet {
            let minLength = min(size.width, size.height)
            self.stampView.layer.cornerRadius = minLength/2.0
            self.stampView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    func setupView() {
        self.stampView.layer.borderColor = UIColor.black.cgColor
        self.stampView.layer.borderWidth = 2
        self.contentView.addSubview(self.stampView)
    }
    
    override var isSelected: Bool {
        didSet {
            self.stampView.backgroundColor = self.isSelected ? UIColor.blue : UIColor.gray
        }
    }
    
}


