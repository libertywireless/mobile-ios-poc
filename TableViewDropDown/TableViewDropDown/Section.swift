//
//  Section.swift
//  TableViewDropDown
//
//  Created by Arun Sivakumar on 18/1/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

import Foundation


struct Section{
    var genre: String!
    var movies:[String]!
    var expanded: Bool!
    
//    init(genre:String, movies: [String], expanded:Bool){
//        self.genre = genre
//        self.movies = movies
//        self.expanded = expanded
//    }
}
