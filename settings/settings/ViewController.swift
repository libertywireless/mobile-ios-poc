//
//  ViewController.swift
//  settings
//
//  Created by Arun Sivakumar on 8/2/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pressed(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string:"App-Prefs:root=MOBILE_DATA_SETTINGS_ID")! as URL)
    }
    
}

