//
//  ViewController.swift
//  ButtonTurn
//
//  Created by Arun Sivakumar on 26/1/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var b: UIButton!
    
    var value = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func click(_ sender: UIButton) {
        if (value){
                b.transform = CGAffineTransform(rotationAngle:  CGFloat.pi)
            value = false
        }else{
            b.transform = CGAffineTransform(rotationAngle:  -CGFloat.pi)
            value = true
        }
    }
    
}

