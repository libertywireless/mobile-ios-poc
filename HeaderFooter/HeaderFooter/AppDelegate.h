//
//  AppDelegate.h
//  HeaderFooter
//
//  Created by Arun Sivakumar on 22/1/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

