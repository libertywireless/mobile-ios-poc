//
//  main.m
//  HeaderFooter
//
//  Created by Arun Sivakumar on 22/1/18.
//  Copyright © 2018 Arun Sivakumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
