//: Playground - noun: a place where people can play

import UIKit

class Gear{
    
    let chainring:Int
    let cog:Int
    let rim:Int
    let tyre:Double
    
    init(chainring:Int,cog:Int,rim:Int,tyre:Double) {
        self.chainring = chainring
        self.cog = cog
        self.rim = rim
        self.tyre = tyre
    }
    
    func ratio() -> Double{
       return Double(self.chainring) / Double(self.cog)
    }
    
    func gearInches() -> Double{
        return ratio() * ((Double(rim) + (tyre * 2)))
    }
    
}


let c1 = Gear(chainring: 52, cog: 11, rim: 26, tyre: 1.5)


let c2 = Gear(chainring: 30, cog: 27, rim: 24, tyre: 1.25)

c1.ratio()
c2.ratio()

c1.gearInches()
c2.gearInches()